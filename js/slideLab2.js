$(function() {
    // 変数定義
    var slider = $('.setLab'),
        container = slider.find('.setLab__center__container'),
        contents = container.children(),
        firstChild = contents.filter(':first-child'),
        lastChild = contents.filter(':last-child');
    var slider2 = $('article'),
        container2 = slider2.find('.tempContentWrapper'),
        contents2 = container2.children(),
        firstChild2 = contents2.filter(':first-child'),
        lastChild2 = contents2.filter(':last-child');

    // スライドが表示されるエリアのサイズ（tempContentWrapper）
    var size = {
        width: container.width()
    };

    // スライドさせるコンテンツの現在地管理
    var count = {
        min: 0,
        max: contents.length,
        current: 0
    };
    // コンテナの前後に１つ分のスペースを作成（ループさせるため）
    container.css({
        width: size.width * (contents.length + 2),
        marginLeft: -size.width,
        paddingLeft: size.width
    });
    container2.css({
        width: size.width * (contents2.length + 2),
        marginLeft: -size.width,
        paddingLeft: size.width
    });

    var distance;
    var slide = {
        // スライド処理（right）
        next: function (index) {
            // 移動距離算出
            fnc.range(index, 'positive');
            if(count.current < count.max - 1) {
                // 現在地が最後のコンテンツより前の場合の処理
                fnc.scroll(distance);
            } else {
                // 現在地が最後のコンテンツだった場合、最初のコンテンツをコンテナの一番後ろまで移動
                firstChild.css('left', size.width * contents.length);
                firstChild2.css('left', size.width * contents2.length);
                container.animate({left: -distance},300,function () {
                    // 移動した最初のコンテンツを戻す
                    firstChild.css('left', 0);
                    // スライドしていったコンテナも戻す
                    container.css('left', 0);
                    });
                container2.animate({left: -distance},300,function() {
                    firstChild2.css('left', 0);
                    container2.css('left', 0);
                })
                // 現在地を-1に設定（次の処理で0となる）
                count.current = -1;
            }
            // 現在地に1足す
            fnc.counter(index, 'increment');
        },
        // スライド処理（left）
        prev: function (index) {
            // 移動距離算出
            fnc.range(index, 'negative');
            // 現在地が最初のコンテンツより進んでいる場合
            if(count.current > count.min) {
                fnc.scroll(distance);
            } else {
                // 現在地が最初のコンテンツだった場合、最後のコンテンツをコンテナの一番前まで移動
                lastChild.css('left', -(size.width * contents.length));
                container.animate({left: -distance},300,function () {
                    // 移動した最後のコンテンツを戻す
                    lastChild.css('left', '');
                    // スライドしていったコンテナも戻す
                    container.css('left', -(size.width * (contents.length - 1)));
                });
                lastChild2.css('left', -(size.width * contents2.length));
                container2.animate({left: -distance},300,function() {
                    lastChild2.css('left', '');
                    container2.css('left', -(size.width * (contents2.length - 1)));
                });
                // カウントの最大値を渡す
                count.current = count.max;
            }
            // 現在地から1引く
            fnc.counter(index, 'decrement');
        }
    }
    var fnc = {
        range: function(n, d) {
            var addNum;
            if(d === 'negative') addNum = -1;   //戻る
            if(d === 'positive') addNum = +1;   //進む
            // 移動させる距離の算出
            distance = size.width * (count.current + addNum);
        },
        scroll: function (d) {
            // 移動距離分のスライド処理
            container.animate({left: -d});
            container2.animate({left: -d});
        },
        // 現在位置の増減
        counter: function(n,c) {
            if(c === 'increment') count.current++;
            if(c === 'decrement') count.current--;
        },
        pager: function (d,e) {
            if(!container.is(':animated')) {
                if(d === 'positive') slide.next();
                if(d === 'negative') slide.prev();
            }
        }
    };
    // leftボタン押下時
    $('.setLab__left').click(function (e) {
        fnc.pager('negative', e);
        // バブリング回避
        return false;
    });
    // rightボタン押下時
    $('.setLab__right').click(function (e) {
        fnc.pager('positive', e);
        return false;
    });

});