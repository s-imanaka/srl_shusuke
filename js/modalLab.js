//ラボ切替えモーダル
$(function() {
    // 表示
    $('.setLab').on('click', function() {
        $('.dialog__bg').addClass('show lab__bg');
        $('#labArea').addClass('show');
		$('.lockWrap').addClass('lock');
    })

    // とじる
    $('.dialog__bg,.closeBtn').on('click', function() {
        $('.dialog__bg').removeClass('show lab__bg');
        $('#labArea').removeClass('show');
		$('.lockWrap').removeClass('lock');
    })
	//.labAreaの子要素である.labArea__wrapperは発火させない。
	$('#labArea__wrapper').click(function(e) {
		var target = e.target;
		if (target.id === 'labArea__wrapper') {
			$('.dialog__bg').removeClass('show lab__bg');
			$('#labArea').removeClass('show');
			$('.lockWrap').removeClass('lock');
		}
    })

    // ラボ選択
	$('input[type="checkbox"]').on('change', function() {
		$(this).parents('.labArea__wrapper__list').toggleClass('selected');
	})
});