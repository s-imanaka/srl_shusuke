$(function() {
    $('.oneBT-daialog').on('click',function() {
        $('.dialog__bg').addClass('show');
        $('#oneBT-daialog').addClass('show');
    })
    $('.twoBT-daialog').on('click', function() {
		$('.dialog__bg').addClass('show');
		$('#twoBT-daialog').addClass('show');
    })
    $('.input-daialog').on('click', function() {
        $('.dialog__bg').addClass('show');
        $('#input-daialog').addClass('show');
    })
    $('.list-daialog').on('click', function() {
        $('.dialog__bg').addClass('show');
        $('#list-daialog').addClass('show');
    })
	//コントロール
    $('.ok').on('click', function() {
        $('.dialog__bg').removeClass('show');
		$('.dialog').removeClass('show');
		console.log('cancel');
    })
    $('.cancel').on('click', function() {
		$('.dialog__bg').removeClass('show');
		$('.dialog').removeClass('show');
		console.log('cancel');
    })
	$('.dialog__bg').on('click', function() {
        $('.dialog__bg').removeClass('show');
		$('.dialog').removeClass('show');
		console.log('cancel');
    })
})

//【ダイアログを利用するHTML内に配置する部品】
//<!--背景黒-->
//<div class="dialog__bg"></div>

//【HTML内に配置するダイアログ種類】
//<!--ダイアログ本体：1ボタンダイアログ-->
//<div class="dialog" id="oneBT-daialog">
//	<div class="dialog__contents">
//		<h4>検体情報</h4>
//		<p>全血凍結項目は、セロトニン・ニコチンサン酸・エタノール・タクロリムス・ビタミンB1・ビタミンB2</p>
//	</div>
//	<div class="dialog__buttonStyle buttonStyle--single">
//		<div class="ok">OK</div>
//	</div>
//</div>

//<!--ダイアログ本体：2ボタンダイアログ-->
//<div class="dialog" id="twoBT-daialog">
//	<div class="dialog__contents">
//		<p>温度入力中にラボを変更すると入力したデータが削除されます。ラボを変更しますか？</p>
//	</div>
//	<div class="dialog__buttonStyle buttonStyle--double">
//		<div class="cancel">キャンセル</div>
//		<div class="ok">OK</div>
//	</div>
//</div>

//<!--ダイアログ本体：インプット欄ありのダイアログ-->
//<div class="dialog" id="input-daialog">
//	<div class="dialog__contents">
//		<h4>血漿 血清</h4>
//		<p>追加本数を入力してください</p>
//		<div class="dialog__contents__item">
//			<span>23</span>
//			<input type="text">
//		</div>
//	</div>
//	<div class="dialog__buttonStyle buttonStyle--double">
//		<div class="cancel">キャンセル</div>
//		<div class="ok">OK</div>
//	</div>
//</div>

//<!--ダイアログ本体：リストスタイルダイアログ（病院別セット詳細用）-->
//<div class="dialog" id="list-daialog">
//	<div class="dialog__contents clinicList">
//		<h4>蛋白定性（部分尿）</h4>
//		<ul class="list list--narrow">
//			<li class="list__content">
//				<dl class="list__content__style list__content__style--bothEnds">
//					<dt class="clinicList__name">コード</dt>
//					<dd class="clinicList__value">1003</dd>
//				</dl>
//			</li>
//			<li class="list__content">
//				<dl class="list__content__style list__content__style--bothEnds">
//					<dt class="clinicList__name">容器</dt>
//					<dd class="clinicList__value">ポリスピッツ</dd>
//				</dl>
//			</li>
//			<li class="list__content list__content--endpoint">
//				<dl class="list__content__style list__content__style--bothEnds">
//					<dt class="clinicList__name">材料</dt>
//					<dd class="clinicList__value">部分尿</dd>
//				</dl>
//			</li>
//		</ul>
//	</div>
//	<div class="dialog__buttonStyle buttonStyle--single clinicButton">
//		<div class="ok">OK</div>
//	</div>
//</div>