$(function() {
    $('.sideMenu').load('menu.html');
    $('input[type="checkbox"]').on('change', function() {
        // メニュー表示処理
        $('.sideMenu').addClass('triggerCheck');
        $('.sideMenu').removeClass('triggerNoCheck');
        $('.dialog__bg').fadeIn(500);

    $('.menuHeader__wrapper__close, .dialog__bg').on('click', function() {
        // メニュー閉じる処理
        $('.sideMenu').removeClass('triggerCheck');
        $('.sideMenu').addClass('triggerNoCheck');
        $('.dialog__bg').fadeOut(500);
        $('#trigger').prop('checked', false);
        })
    })
})